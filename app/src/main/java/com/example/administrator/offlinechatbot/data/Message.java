package com.example.administrator.offlinechatbot.data;

import android.provider.BaseColumns;


public class Message {

    private long _id;
    private int type;
    private String body;
    private String timeStamp;

    public Message(){}
    public Message(int type,String body,String timeStamp){
        this.setType(type);
        this.setBody(body);
        this.setTimeStamp(timeStamp);
    }
    public Message(long _id,int type,String body,String timeStamp){
        this.set_id(_id);
        this.setType(type);
        this.setBody(body);
        this.setTimeStamp(timeStamp);
    }



    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public interface Table_Message extends BaseColumns {
        String tableName = Message.class.getSimpleName();
        String columnType = "messageType";
        String columnNameType = "INTEGER";
        String columnBody = "body";
        String columnBodyType = "TEXT";
        String columnTime = "timeStamp";
        String columnTimeType = "TEXT";
    }
}
