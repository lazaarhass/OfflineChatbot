package com.example.administrator.offlinechatbot.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SQLHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "database.db";
    private static final int DB_VERSION = 1;
    private static final String CREATE_TABLE_INTENT = "CREATE TABLE " + Intent.Table_Intent.tableName + " ( " +
            Intent.Table_Intent._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Intent.Table_Intent.columnName + " " + Intent.Table_Intent.columnNameType + "," + Intent.Table_Intent.columnResponse
            +" "+ Intent.Table_Intent.columnResponseType + ")";

    private static final String CREATE_TABLE_MESSAGE = "CREATE TABLE " + Message.Table_Message.tableName + " ( " +
            Message.Table_Message._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Message.Table_Message.columnType + " " + Message.Table_Message.columnNameType + "," + Message.Table_Message.columnBody
            +" "+ Message.Table_Message.columnBodyType + "," + Message.Table_Message.columnTime + " "+ Message.Table_Message.columnTimeType + ")";

    private static final String DROP_INTENT = "DROP TABLE " + Intent.Table_Intent.tableName;
    private static final String DROP_MESSAGE = "DROP TABLE " + Intent.Table_Intent.tableName;


    private static SQLHelper instance;



    private SQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static SQLHelper getInstance(Context context) {
        return instance != null ? instance : (instance = new SQLHelper(context));
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_INTENT);
        db.execSQL("insert into " + Intent.Table_Intent.tableName + "(" + Intent.Table_Intent.columnName  + ","
                + Intent.Table_Intent.columnResponse + ") values('Default Fallback Intent','Je n''ai pas bien compris ce que vous vouliez-dire. ')");

        db.execSQL("insert into " + Intent.Table_Intent.tableName + "(" + Intent.Table_Intent.columnName  + ","
                + Intent.Table_Intent.columnResponse + ") values('Brancher un voltmètre','On utilise la borne COM et une borne de tension (V)\n" +
                "Le sens de branchement : La borne COM est la plus proche de la borne négative et la borne de tension la plus proche de la borne positive du générateur.\n" +
                "L''insertion dans le circuit : Le multimètre est branché en dérivation (Le multimètre est branché aux deux bornes du dipôle dont on veut connaitre la tension reçue).\n" +
                "Voulez-vous un diagramme d''aide ?')");

        db.execSQL("insert into " + Intent.Table_Intent.tableName + "(" + Intent.Table_Intent.columnName  + ","
                + Intent.Table_Intent.columnResponse + ") values('Brancher un ampèremètre','L''ampèremètre se branche en série dans le circuit. C''est un dipôle dont la borne COM (« COMmune », dans le cas d''un multimètre)se branche vers la borne moins (-) du générateur. Comment brancher convenablement un ampèremètre dans un circuit électrique.')");

        db.execSQL("insert into " + Intent.Table_Intent.tableName + "(" + Intent.Table_Intent.columnName  + ","
                + Intent.Table_Intent.columnResponse + ") values('Brancher un ohmmètre','Pour effectuer une lecture avec un ohmmètre, il faut le brancher  directement aux bornes du composant préalablement débranché. Il ne faut jamais mesurer une résistance sous tension. La lecture en serait fausse et l''appareil de mesure pourrait être endommagé.')");

        db.execSQL("insert into " + Intent.Table_Intent.tableName + "(" + Intent.Table_Intent.columnName  + ","
                + Intent.Table_Intent.columnResponse + ") values('Voir la liste','La liste des équipements :\n" +
                "1- Amperemetre \n" +
                "2- Voltmetre\n" +
                "3- Ohmmetre')");
        db.execSQL(CREATE_TABLE_MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_INTENT);
        db.execSQL(DROP_MESSAGE);
        onCreate(db);
    }
}
