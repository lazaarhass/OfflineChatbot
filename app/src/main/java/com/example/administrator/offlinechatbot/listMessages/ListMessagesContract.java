package com.example.administrator.offlinechatbot.listMessages;

import android.support.annotation.NonNull;

import com.example.administrator.offlinechatbot.data.Intent;
import com.example.administrator.offlinechatbot.data.Message;

import java.util.List;

/**
 * Created by Administrator on 14/02/2018.
 */

public interface ListMessagesContract {

    interface View {

        void showMessages(List<Message> messages);
    }

    interface UserActionsListener {
        List<Intent> searchIntentByName(@NonNull String s);
        Intent findOneIntent(long id);
        List<Message> getAllMessages();
        List<Message> getLastMessages();
        long addMessage(@NonNull Message message);
    }
}
