package com.example.administrator.offlinechatbot.data;


import android.provider.BaseColumns;

public class Intent {
    private long _id;
    private String intentName;
    private String response;

    public Intent(){}
    public Intent(String intentName, String response){
        this.setIntentName(intentName);
        this.setResponse(response);
    }
    public Intent(long _id, String intentName, String response){
        this.set_id(_id);
        this.setIntentName(intentName);
        this.setResponse(response);
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getIntentName() {
        return intentName;
    }

    public void setIntentName(String intentName) {
        this.intentName = intentName;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public interface Table_Intent extends BaseColumns {
        String tableName = Intent.class.getSimpleName();
        String columnName = "intentName";
        String columnNameType = "TEXT";
        String columnResponse = "response";
        String columnResponseType = "TEXT";

    }

}
