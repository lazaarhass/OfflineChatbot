package com.example.administrator.offlinechatbot.listMessages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.administrator.offlinechatbot.data.Intent;
import com.example.administrator.offlinechatbot.R;
import com.example.administrator.offlinechatbot.data.Message;
import com.example.administrator.offlinechatbot.data.MessagesAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListMessagesFragment extends Fragment implements ListMessagesContract.View{

    private ListMessagesContract.UserActionsListener mUserActionsListener;
    private MessagesAdapter mMessagesAdapter;
    private RecyclerView mRecyclerView;
    private Button mSendButton;
    private EditText mMessageBody;
    private List<Message> mMessageList;


    public ListMessagesFragment() {
        // Required empty public constructor
    }
    public static ListMessagesFragment newInstance() { return new ListMessagesFragment();}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserActionsListener = ListMessagesPresenter.getInstance(getContext());
        mMessageList = new ArrayList<>();
        mMessageList = mUserActionsListener.getLastMessages();
        mMessagesAdapter = new MessagesAdapter(getContext(),mMessageList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_list_messages, container, false);
        mRecyclerView = root.findViewById(R.id.message_list);
        mSendButton = root.findViewById(R.id.button_chatbox_send);
        mSendButton.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick ( View v ) {
                sendButtonClicked(v);
            }
        } ) ;
        mMessageBody = root.findViewById(R.id.editText_chatbox);
        LinearLayoutManager layoutManager =new LinearLayoutManager(getContext());
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mMessagesAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.smoothScrollToPosition(mMessagesAdapter.getItemCount());
        return root;
    }


    private void sendButtonClicked(View v) {
        Message messageChatbot;
        Message messageUser=new Message(1,mMessageBody.getText().toString(),getCurrentTimeStamp());
        List<Intent> responseList = mUserActionsListener.searchIntentByName(mMessageBody.getText().toString());

        if(responseList.size()>0) {
            messageChatbot = new Message(2, responseList.get(responseList.size()-1).getResponse(), getCurrentTimeStamp());
        }
        else{
            messageChatbot= new Message(2,mUserActionsListener.findOneIntent(1).getResponse(),getCurrentTimeStamp());
        }
        mUserActionsListener.addMessage(messageUser);
        long lastPosition = mUserActionsListener.addMessage(messageChatbot);
        mMessageBody.setText("");
        mRecyclerView.smoothScrollToPosition((int)lastPosition);
        mMessageList.add(messageUser);
        mMessageList.add(messageChatbot);
        showMessages(mMessageList);
    }

    public static String getCurrentTimeStamp(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            return dateFormat.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void showMessages(List<Message> messages) {
        mMessagesAdapter.replaceData(messages);
    }

}
