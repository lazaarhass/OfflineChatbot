package com.example.administrator.offlinechatbot.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.offlinechatbot.R;


import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_TYPE_MESSAGE_USER = 1;
    private static final int VIEW_TYPE_MESSAGE_CHATBOT = 2;

    private Context mContext;
    private List<Message> mMessageList;


    public MessagesAdapter(Context context, List<Message> messageList) {
        mContext = context;
        mMessageList = messageList;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {

            Message message = mMessageList.get(position);
            if (message.getType() == 1) {
                return VIEW_TYPE_MESSAGE_USER;
            } else if (message.getType() == 2) {
                return VIEW_TYPE_MESSAGE_CHATBOT;
            } else {
                return -1;
            }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_MESSAGE_USER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_user, parent, false);
            return new ViewHolderOne(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_CHATBOT) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_chatbot, parent, false);
            return new ViewHolderTwo(view);
        } else {
            throw new RuntimeException("Unknown Type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_USER:
                initLayoutOne((ViewHolderOne)holder, position);
                break;
            case VIEW_TYPE_MESSAGE_CHATBOT:
                initLayoutTwo((ViewHolderTwo) holder, position);
                break;
            default:
                break;
        }
    }
    private void initLayoutOne(ViewHolderOne holder, int pos) {
        holder.messageBody.setText(mMessageList.get(pos).getBody());
        holder.messageTime.setText(mMessageList.get(pos).getTimeStamp());
    }

    private void initLayoutTwo(ViewHolderTwo holder, int pos) {
        holder.messageBody.setText(mMessageList.get(pos).getBody());
        holder.messageTime.setText(mMessageList.get(pos).getTimeStamp());
    }

    public void replaceData(List<Message> messages) {
        mMessageList = messages;
        notifyDataSetChanged();
    }

    static class ViewHolderOne extends RecyclerView.ViewHolder {
        public TextView messageBody,messageTime;
        public ViewHolderOne(View itemView) {
            super(itemView);
            messageBody =  itemView.findViewById(R.id.text_message_body1);
            messageTime =  itemView.findViewById(R.id.text_message_time1);
        }
    }

    static class ViewHolderTwo extends RecyclerView.ViewHolder {
        public TextView messageBody,messageTime;
        public ViewHolderTwo(View itemView) {
            super(itemView);
            messageBody =  itemView.findViewById(R.id.text_message_body2);
            messageTime =  itemView.findViewById(R.id.text_message_time2);
        }
    }

}
