package com.example.administrator.offlinechatbot.listMessages;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.administrator.offlinechatbot.R;

public class ListMessagesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_messages);

        if (null == savedInstanceState) {
            initFragment(ListMessagesFragment.newInstance());
        }
    }


    private void initFragment(Fragment listMessagesFragment) {
        // Add the ListMessagesFragment to the layout
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.contentFrame, listMessagesFragment);
        transaction.commit();
    }
}
