package com.example.administrator.offlinechatbot.listMessages;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.administrator.offlinechatbot.data.Intent;
import com.example.administrator.offlinechatbot.data.Message;
import com.example.administrator.offlinechatbot.data.SQLHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Administrator on 14/02/2018.
 */

public class ListMessagesPresenter implements ListMessagesContract.UserActionsListener{

    private static ListMessagesPresenter mInstance;
    private SQLHelper mSqlHelper;

    private ListMessagesPresenter(Context context) {
        mSqlHelper = SQLHelper.getInstance(context);
    }
    public static ListMessagesPresenter getInstance(Context context) {
        return mInstance != null ? mInstance : (mInstance = new ListMessagesPresenter(context));
    }

    @Override
    @NonNull
    public List<Intent> searchIntentByName(@NonNull String s) {
        List<Intent> items = new LinkedList<>();
        SQLiteDatabase db = mSqlHelper.getReadableDatabase();
        Cursor cursor = db.query(
                Intent.Table_Intent.tableName,
                null,
                Intent.Table_Intent.columnName + " LIKE ?",
                new String[]{"%"+ s +"%"}, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            items.add(new Intent(cursor.getLong(0), cursor.getString(1),cursor.getString(2)));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return items;
    }
    @Override
    @Nullable
    public Intent findOneIntent(long id) {
        SQLiteDatabase db = mSqlHelper.getReadableDatabase();
        Cursor cursor = db.query(
                Intent.Table_Intent.tableName,
                null,
                Intent.Table_Intent._ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null);
        cursor.moveToFirst();
        Intent item = null;
        if (!cursor.isAfterLast()) {
            item = new Intent(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
        }
        cursor.close();
        db.close();
        return item;
    }
    @Override
    @NonNull
    public List<Message> getAllMessages() {
        List<Message> items = new LinkedList<>();
        SQLiteDatabase db = mSqlHelper.getReadableDatabase();
        Cursor cursor = db.query(
                Message.Table_Message.tableName,
                null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            items.add(new Message(cursor.getLong(0), cursor.getInt(1), cursor.getString(2),cursor.getString(3)));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return items;
    }
    @Override
    @NonNull
    public List<Message> getLastMessages(){
        List<Message> items = new LinkedList<>();
        SQLiteDatabase db = mSqlHelper.getReadableDatabase();
        final String aquery = "select * from (select * from "+ Message.Table_Message.tableName +" order by "+ Message.Table_Message._ID +" DESC limit 10) order by "+Message.Table_Message._ID +" ASC;";
        Cursor cursor = db.rawQuery(aquery,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            items.add(new Message(cursor.getLong(0), cursor.getInt(1), cursor.getString(2),cursor.getString(3)));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return items;
    }

    @Override
    @NonNull
    public long addMessage(@NonNull Message el) {
        SQLiteDatabase database = mSqlHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Message.Table_Message.columnType, el.getType());
        contentValues.put(Message.Table_Message.columnBody,el.getBody());
        contentValues.put(Message.Table_Message.columnTime,el.getTimeStamp());
        long id = database.insert(Message.Table_Message.tableName, null, contentValues);
        database.close();
        return id;
    }
}
